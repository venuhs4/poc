﻿(function () {
    "use strict";

    angular.module("myapp.controllers", ['ionic', 'chart.js', 'ngCordova'])

    .controller("appCtrl", ["$scope", function ($scope) {
    }])
    .controller("homeCtrl", ["$scope", "$state", "$http", "$ionicPopup", "$ionicLoading", function ($scope, $state, $http, $ionicPopup, $ionicLoading) {

        $scope.refresh = function () {
            //refresh binding
            $scope.$broadcast("scroll.refreshComplete");
        };
        $scope.data = {
            lot: ''
        };
        $scope.Submit = function () {
            $ionicLoading.show({ template: "Processing..." });
            $http.get('http://10.252.201.52/apitest/test/ValidateLot?container=' + $scope.data.lot).then(function (res) {
                console.log(res);
                if (res.data.IsSuccess !== undefined) {
                    $scope.data.result = res.data.Message;
                    $scope.data.style = 'Success';
                }
                else if (res.data.ExceptionData !== undefined) {
                    $scope.data.result = res.data.ExceptionData.Description;
                    $scope.data.style = 'Fail';
                }
                else {
                    $scope.data.result = "Connection Error!";
                    $scope.data.style = 'Error';
                }
                $ionicLoading.hide();
                $ionicPopup.alert({ title: $scope.data.style, template: $scope.data.result });
            }, function (err) {
                $scope.data.result = "Connection Error!";
                $scope.data.style = 'Error';
                console.log(err);
                $ionicLoading.hide();
            });
        };
    }])
    .controller("stockerTxnCtrl", ["$scope", "$state", "$http", "$ionicPopup", "$ionicLoading", function ($scope, $state, $http, $ionicPopup, $ionicLoading) {
        $scope.refresh = function () {
            //refresh binding
            $scope.$broadcast("scroll.refreshComplete");
        };
        $scope.data = {
            lot: ''
        };
        $scope.Submit = function () {
            var reqData = {
                Container: $scope.data.lot,
                Stocker: $scope.data.stocker,
                Comments: $scope.data.Comments,
                TxnType: $scope.stockerInBt === 'button-positive' ? 'IN' : 'OUT'
            };
            $ionicLoading.show({ template: "Processing..." });
            $http.get('http://10.252.201.52/apitest/test/StockerTxn?input=' + JSON.stringify(reqData)).then(function (res) {
                console.log(res);
                if (res.data.IsSuccess !== undefined) {
                    $scope.data.result = res.data.Message;
                    $scope.data.style = 'Success';
                }
                else if (res.data.ExceptionData !== undefined) {
                    $scope.data.result = res.data.ExceptionData.Description;
                    $scope.data.style = 'Fail';
                }
                else {
                    $scope.data.result = "Connection Error!";
                    $scope.data.style = 'Error';
                }
                $ionicPopup.alert({ title: $scope.data.style, template: $scope.data.result });
                $ionicLoading.hide();
            }, function (err) {
                $scope.data.result = "Connection Error!";
                $scope.data.style = 'Error';
                console.log(err);
                $ionicLoading.hide();
            });
        };

        $scope.stockerInBt = 'button-positive';
        $scope.stockerOutBt = 'button-light';
        $scope.stockerIn = function () {
            $scope.stockerInBt = 'button-positive';
            $scope.stockerOutBt = 'button-light';
        };
        $scope.stockerOut = function () {
            $scope.stockerInBt = 'button-light';
            $scope.stockerOutBt = 'button-positive';
        };
    }])
    .controller("lotDetailCtrl", ["$scope", "$state", "$http", "$ionicPopup", "$ionicLoading", function ($scope, $state, $http, $ionicPopup, $ionicLoading) {
        $scope.refresh = function () {
            //refresh binding
            $scope.$broadcast("scroll.refreshComplete");
        };
        $scope.data = {
            lot: ''
        };
        $scope.Submit = function () {
            $ionicLoading.show({ template: "Processing..." });
            $http.get('http://10.252.201.52/apitest/test/GetLotDetails?container=' + $scope.data.lot).then(function (res) {
                console.log(res);
                if (res.data.ExceptionData !== undefined) {
                    $scope.data.result = res.data.ExceptionData.Description;
                    $scope.data.style = 'Fail';
                    $ionicPopup.alert({ title: $scope.data.style, template: $scope.data.result });
                }
                else {
                    $scope.data.result = res.data;
                    $scope.data.style = 'Success';
                }

                $ionicLoading.hide();
            }, function (err) {
                $scope.data.result = "Connection Error!";
                $scope.data.style = 'Error';
                console.log(err);
                $ionicLoading.hide();
            })
        };
    }])
    .controller("loginCtrl", ["$scope", "$state", "$http", "$ionicPopup", "$ionicLoading", "$cordovaBarcodeScanner",
    function ($scope, $state, $http, $ionicPopup, $ionicLoading, $cordovaBarcodeScanner) {

        $scope.refresh = function () {
            //refresh binding
            $scope.$broadcast("scroll.refreshComplete");
        };
        $scope.data = {
            lot: ''
        };
        $scope.Submit = function () {
            $ionicLoading.show({ template: "Processing..." });
            var reqData = {
                Username: $scope.data.username,
                password: $scope.data.password,
            };
            $http.get('http://10.252.201.52/apitest/test/Login?input=' + JSON.stringify(reqData)).then(function (res) {
                console.log(res);
                if (res.data.IsSuccess !== undefined) {
                    $scope.data.result = res.data.Message;
                    $scope.data.style = 'Success';
                    $state.go('app.home');
                }
                else if (res.data.ExceptionData !== undefined) {
                    $scope.data.result = res.data.ExceptionData.Description;
                    $scope.data.style = 'Fail';
                }
                else {
                    $scope.data.result = "Connection Error!";
                    $scope.data.style = 'Error';
                }
                $ionicLoading.hide();
                $ionicPopup.alert({ title: $scope.data.style, template: $scope.data.result });
            }, function (err) {
                $scope.data.result = "Connection Error!";
                $scope.data.style = 'Error';
                console.log(err);
                $ionicLoading.hide();
            });
        };
        $scope.scan = function () {
            console.log($cordovaBarcodeScanner);
        };
    }])
    .controller("barcodeCtrl", ["$scope", "$state", "$http", "$ionicPopup", "$ionicLoading", "$cordovaBarcodeScanner",
    function ($scope, $state, $http, $ionicPopup, $ionicLoading, $cordovaBarcodeScanner) {

        $scope.refresh = function () {
            //refresh binding
            $scope.$broadcast("scroll.refreshComplete");
        };
        $scope.data = {
            lot: ''
        };

        $scope.scanLot = function () {
            $cordovaBarcodeScanner
            .scan()
            .then(function (barcodeData) {
                if (!barcodeData.cancelled)
                {
                    $scope.data.barcodetext = barcodeData.text;
                }
            }, function (error) {
                console.log(error);
            });
        };
    }])
    .controller("chartCtrl", ['$scope', '$state', '$interval', function ($scope, $state, $interval) {
        console.log('chartCtrl');
        $scope.status = {
            refreshBtn: 'Auto Refresh Off'
        };
        $scope.num = 10;
        $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
        $scope.series = ['Series A', 'Series B'];

        $scope.data = [
            [10, 59, 80, $scope.num, 56, 55, 40],
            [28, 48, 40, 19, 86, 27, 90]
        ];
        var changeFun;
        $scope.refreshBtnClick = function () {
            if ($scope.status.refreshBtn === 'Auto Refresh Off') {
                $scope.status.refreshBtn = 'Auto Refresh On';
                $scope.refreshOn();
            }
            else {
                $scope.status.refreshBtn = 'Auto Refresh Off';
                $scope.refreshOff();
            }
        };
        $scope.refreshOn = function () {
            if (angular.isDefined(changeFun))
                return;
            changeFun = $interval(function () {
                $scope.num = $scope.num + 10;
                $scope.data = [
            [10, 59, 80, $scope.num, 56, 55, 40],
            [28, 48, 40, 19, 45, 27, 90]
                ];
                console.log($scope.num + new Date());
            }, 5000);
        };
        $scope.refreshOff = function () {
            if (angular.isDefined(changeFun)) {
                $interval.cancel(changeFun);
                changeFun = undefined;
            }
        };
    }])
    .controller("lotOperationCtrl", ['$scope', '$state', '$interval', function ($scope, $state, $interval) {
        $scope.status = {
            refreshBtn: 'Auto Refresh Off'
        };

        var res = '[{"labels":["A WET","ALCU_SPUTTER","ALIGN","ASHING","BARC"],"data":[[4,1,41,74,148]]},{"labels":["BARC_ETCH","BASE_STOCK","COAT","COAT/TARC","DEVELOP"],"data":[[4,1,69,1,15]]},{"labels":["H2 ANNEAL","INV001","LASER_NAMING","OXIDATION","PARTICLE_INSPECT"],"data":[[11,5,101,15,2]]},{"labels":["PRE_CLEAN","PROBE_INSPECT(3)","PTEOS","RESIST","SIN_DEPO"],"data":[[54,1,5,11,7]]},{"labels":["SIN_ETCH","SIN_REMOVAL","TEST_GFOPE1","THICKNESS_MEAS","U+D_CLEAN"],"data":[[1,1,4,26,1]]},{"labels":["WAFER_INPUT","WAFER_INPUT_BRN","WATERCLEAN"],"data":[[434,1,3]]}]';

        $scope.reports = JSON.parse(res);

        $scope.series = ['Operations'];

        var changeFun;
        $scope.refreshBtnClick = function () {
            if ($scope.status.refreshBtn === 'Auto Refresh Off') {
                $scope.status.refreshBtn = 'Auto Refresh On';
                $scope.refreshOn();
            }
            else {
                $scope.status.refreshBtn = 'Auto Refresh Off';
                $scope.refreshOff();
            }
        };
        $scope.refreshOn = function () {
            if (angular.isDefined(changeFun))
                return;
            changeFun = $interval(function () {
                // update chart data
            }, 5000);
        };
        $scope.refreshOff = function () {
            if (angular.isDefined(changeFun)) {
                $interval.cancel(changeFun);
                changeFun = undefined;
            }
        };
    }])
    .controller("errorCtrl", ["$scope", "myappService", function ($scope, myappService) {
        //public properties that define the error message and if an error is present
        $scope.error = "";
        $scope.activeError = false;

        //function to dismiss an active error
        $scope.dismissError = function () {
            $scope.activeError = false;
        };

        //broadcast event to catch an error and display it in the error section
        $scope.$on("error", function (evt, val) {
            //set the error message and mark activeError to true
            $scope.error = val;
            $scope.activeError = true;

            //stop any waiting indicators (including scroll refreshes)
            myappService.wait(false);
            $scope.$broadcast("scroll.refreshComplete");

            //manually apply given the way this might bubble up async
            $scope.$apply();
        });
    }]);
})();
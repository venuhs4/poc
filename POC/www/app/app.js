﻿(function () {
    "use strict";

    angular.module("myapp", ["ionic", "myapp.controllers", "myapp.services"])
        .run(function ($ionicPlatform) {
            $ionicPlatform.ready(function () {
                if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }
            });
        })
        .config(function ($stateProvider, $urlRouterProvider) {
            $stateProvider
            .state("app", {
                url: "/app",
                abstract: true,
                templateUrl: "app/templates/view-menu.html",
                controller: "appCtrl"
            })
            .state("app.home", {
                url: "/home",
                templateUrl: "app/templates/view-home.html",
                controller: "homeCtrl"
            }).state("app.stockerTxn", {
                url: "/stockerTxn",
                templateUrl: "app/templates/view-stocker-in.html",
                controller: "stockerTxnCtrl"
            }).state("app.lotDeatail", {
                url: "/lotDetail",
                templateUrl: "app/templates/view-lot-detail.html",
                controller: "lotDetailCtrl"
            }).state("login", {
                url: "/login",
                templateUrl: "app/templates/login.html",
                controller: "loginCtrl"
            }).state("app.chart", {
                url: "/chart",
                templateUrl: "app/templates/view-chart.html",
                controller: "chartCtrl"
            }).state("app.lotOperation", {
                url: "/lotOperation",
                templateUrl: "app/templates/view-operation-report.html",
                controller: "lotOperationCtrl"
            }).state("app.allReports", {
                url: "/allreports",
                templateUrl: "app/templates/view-report-list.html"
            }).state('barcodescan', {
                url: "/barcodescan",
                templateUrl: "app/templates/view-barcode-scan.html",
                controller: "barcodeCtrl"
            })
            ;
            //$urlRouterProvider.otherwise("app/chart");
            $urlRouterProvider.otherwise("/barcodescan");
        });
})();